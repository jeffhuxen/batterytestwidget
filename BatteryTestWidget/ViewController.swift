//
//  ViewController.swift
//  BatteryTestWidget
//
//  Created by Jeff Huxen on 8/5/19.
//  Copyright © 2019 Truce Software. All rights reserved.
//
import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var endButton: UIButton!
    @IBOutlet weak var writeSwitch: UISwitch!
    
    var batteryHelper: BatteryHelper! = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.endButton.isEnabled = false
        self.startButton.isEnabled = true
    }
    
    @IBAction func settingsMenu(sender: UIButton?)
    {
        
    }
    
    @IBAction func printDebugLog(sender: UIButton?)
    {
        
    }
    
    @IBAction func beginTests()
    {
        self.startButton.isEnabled = false
        self.endButton.isEnabled = true
        self.writeSwitch.isEnabled = false
        self.batteryHelper = BatteryHelper.init()
        self.batteryHelper.beginTest(fileLoggingEnabled: self.writeSwitch.isOn)
    }
    
    @IBAction func endTests()
    {
        self.endButton.isEnabled = false
        self.startButton.isEnabled = true
        self.writeSwitch.isEnabled = true
        self.batteryHelper.loggingIsEnabled = false
    }
}


