//
//  BatteryHelper.swift
//  Xanadu
//
//  Created by Jeff Huxen on 7/13/19.
//  Copyright © 2019 Joseph Breaux. All rights reserved.
//

import UIKit
import CoreBluetooth

@objc
class BatteryHelper: NSObject, CBCentralManagerDelegate, CBPeripheralDelegate {
    
    var logFileName: String! = nil
    var dateStarted: Date! = nil
    var timeFormatter: DateFormatter! = nil
    var dateFormatter: DateFormatter! = nil
    var myCentralManager: CBCentralManager! = nil
    var myUUID: CBUUID! = nil
    var myPeripheral: CBPeripheral! = nil
    
    // is write to file enabled?
    var loggingIsEnabled: Bool = false
    
    // returns current time as string
    var timeStr: String
    {
        if self.timeFormatter == nil {
            self.timeFormatter = DateFormatter.init()
            self.timeFormatter.timeZone = .current
            self.timeFormatter.dateFormat = "HH.mm.ss"
        }
        return self.timeFormatter.string(from: Date.init(timeIntervalSinceNow: 0))
    }
    
    // returns current date as string
    var dateStr: String
    {
        if self.dateFormatter == nil {
            self.dateFormatter = DateFormatter.init()
            self.dateFormatter.timeZone = .autoupdatingCurrent
            self.dateFormatter.dateFormat = "MM-dd-yy"
        }
        return self.dateFormatter.string(from: Date.init(timeIntervalSinceNow: 0))
    }
    
    var timeStamp: String {
        return timeStr+"_"+dateStr+":>"
    }
    
    // returns battery level as float
    var currentBatteryLevel: Float
    {
        return UIDevice.current.batteryLevel
    }
    
    // returns battery state
    var batteryState: UIDevice.BatteryState
    {
        return UIDevice.current.batteryState
    }
    
    // file url of the current log file
    var fileURL:URL? {
        let dir = FileManager.default.urls(for: .documentDirectory,
                                           in: .userDomainMask).first
        return dir?.appendingPathComponent(logFileName)
    }
    
    override init()
    {
        super.init()
        
        // Add observer for battery level notifications
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(batteryLevelChanged),
                                               name: UIDevice.batteryLevelDidChangeNotification,
                                               object: nil)
        
        // Add observer for battery state notification (plugged in, etc)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(batteryStateChanged),
                                               name: UIDevice.batteryStateDidChangeNotification,
                                               object: nil)
        
        self.myUUID = CBUUID.init(string:"AF2B4311-D70F-4DA3-BC4F-CDC014EB1B08")
        self.myCentralManager = CBCentralManager.init(delegate: self, queue: nil)
    }
    
    // helper method to attach log with timestamp
    func logChange(logMsg: String)
    {
        let log = "\(timeStamp)>> \(logMsg)\n"
        print(log)
        if loggingIsEnabled == true {
            writeLogToFile(logMsg: log)
        }
    }
    
    // create a new log file
    func createNewLogFile()
    {
        // Log file test header
        let headerStr = """
        Begin Testing for: \(UIDevice.current.name)
        Model: \(UIDevice.current.localizedModel)
        OS: \(UIDevice.current.systemName)
        Version: \(UIDevice.current.systemVersion)
        Start Time: \(NSDate.init(timeIntervalSinceNow: 0))
        \n
        """
        
        // Create Initial Log File
        do {
            try headerStr.write(to: fileURL!, atomically: false, encoding: .utf8)
        }catch {
            fatalError("Error: cannot create log file:\(String(describing: fileURL))")
        }
    }
    
    // writes a log message to the log file
    func writeLogToFile(logMsg: String)
    {
        if let fh = try? FileHandle.init(forUpdating: fileURL!)   {
            fh.seekToEndOfFile()
            fh.write(logMsg.data(using: String.Encoding.utf8)!)
            fh.closeFile()
        }else {
            print("Error: Failed writing log to file.")
        }
    }
    
    // creates a unique file name from phone name and timestamp
    func createUniqueFileName() -> String
    {
        return "BattLog_"+dateStr+"_"+timeStr+".log"
    }
    
    // init method to start a new battery helper
    func beginTest(fileLoggingEnabled: Bool)
    {
        // enable battery monitoring
        UIDevice.current.isBatteryMonitoringEnabled = true
        // set start date (timestamp) of test
        self.dateStarted = Date(timeIntervalSinceNow: 0)
        self.loggingIsEnabled = fileLoggingEnabled
        // create a new log file if write is enabled
        if (fileLoggingEnabled == true) {
            self.logFileName = createUniqueFileName()
            self.createNewLogFile()
        }
        batteryLevelChanged()
    }
    
    // Listen for changes in batter level
    @objc func batteryLevelChanged()
    {
        self.logChange(logMsg: "Battery Level:\(currentBatteryLevel)")
    }
    
    // Listen for battery state change and convert to message
    @objc func batteryStateChanged() -> String
    {
        var stateMessage:String! = nil
        switch batteryState {
        case .unplugged:
            stateMessage = "Battery State: unplugged"
        case .charging:
            stateMessage = "Battery State: charging"
        case .full:
            stateMessage = "Battery State: full"
        case .unknown:
            stateMessage = "Battery State: unknown"
        @unknown default:
            fatalError("Error: unknown case in state switch")
        }
        self.logChange(logMsg: stateMessage)
        return stateMessage
    }
    
    //MARK: - CBManagerDelegate methods
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        var btState = "Bluetooth state:"
        switch central.state    {
        case .poweredOff:
            btState+="powered off"
        case .poweredOn:
            btState+="powered on"
            self.myCentralManager.scanForPeripherals(withServices: [myUUID], options: nil)
        case .resetting:
            btState+="resetting"
        case .unauthorized:
            btState+="unauthorized"
        case .unknown:
            btState+="unknown"
        case .unsupported:
            btState+="unsupported"
        @unknown default:
            fatalError("\n\nError:Unknown central.state in delegate's update switch.")
        }
        print(btState)
    }
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        print("Discovered \(peripheral.name ?? "No Name")")
        self.myPeripheral = peripheral
        myCentralManager.stopScan()
        myCentralManager.connect(self.myPeripheral, options: nil)
    }
    
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        print("New BLE peripheral connected.")
        self.myPeripheral.delegate = self
        self.myPeripheral.discoverServices([myUUID])
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        for cbService in peripheral.services! {
            print("Discovered service %@", cbService)
            if cbService.uuid == myUUID {
                peripheral.discoverCharacteristics([myUUID], for: cbService)
            }
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor
        service: CBService, error: Error?) {
        for characteristic in service.characteristics!   {
            print("Discovered characteristic:\(characteristic)")
            self.myPeripheral.setNotifyValue(true, for: characteristic)
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didModifyServices invalidatedServices: [CBService]) {
        print(invalidatedServices)
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateNotificationStateFor characteristic: CBCharacteristic, error: Error?) {
        print(error ?? "No Error")
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        self.batteryLevelChanged()
        print(characteristic.value?.debugDescription ?? error.debugDescription)
    }
}
